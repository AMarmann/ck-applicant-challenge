<?php


namespace App\Dto;


use InvalidArgumentException;

class OverwriteToDo
{
    public int $id;
    public string $name;
    public string $description;
    public array $tasks;

    /**
     * OverwriteToDo constructor.
     * @param int $id
     * @param string $name
     * @param string $description
     * @param array $tasks
     */
    public function __construct(int $id, string $name, string $description, array $tasks)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->tasks = $tasks;
    }

    /**
     * @param int $id
     * @param $validRequestContent
     * @return CreateToDo
     */
    public static function createFromRequestContent(int $id, $validRequestContent): OverwriteToDo
    {
        return new self($id, $validRequestContent['name'], $validRequestContent['description'], $validRequestContent['tasks']);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }
}