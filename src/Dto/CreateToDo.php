<?php


namespace App\Dto;

use InvalidArgumentException;

class CreateToDo
{
    public string $name;
    public string $description;
    public array $tasks;

    /**
     * CreateToDo constructor.
     * @param string $name
     * @param string $description
     * @param array $tasks
     */
    public function __construct(string $name, string $description, array $tasks)
    {
        $this->name = $name;
        $this->description = $description;
        $this->tasks = $tasks;
    }

    /**
     * @param $validRequestContent
     * @return CreateToDo
     */
    public static function createFromRequestContent($validRequestContent): CreateToDo
    {
        return new self($validRequestContent['name'], $validRequestContent['description'], $validRequestContent['tasks']);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }
}