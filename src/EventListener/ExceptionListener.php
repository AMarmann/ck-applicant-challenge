<?php


namespace App\EventListener;


use App\Exception\ToDoNotFoundException;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use TypeError;

/**
 * Class ExceptionListener
 */
class ExceptionListener
{
    /**
     * Create a JsonResponse according to an exception event.
     *
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        switch (true) {
            case $event->getThrowable() instanceof TypeError:
                $event->setResponse(
                    new JsonResponse([
                        'error' => 'Bad Request',
                        'status' => Response::HTTP_BAD_REQUEST,
                        'message' => $event->getThrowable()->getMessage()
                    ], Response::HTTP_BAD_REQUEST)
                );
                break;
            case $event->getThrowable() instanceof ToDoNotFoundException:
                $event->setResponse(
                    new JsonResponse([
                        'error' => 'To-Do not found',
                        'status' => Response::HTTP_NOT_FOUND,
                        'message' => $event->getThrowable()->getMessage()
                    ], Response::HTTP_NOT_FOUND)
                );
                break;
            case $event->getThrowable() instanceof InvalidArgumentException:
                $event->setResponse(
                    new JsonResponse([
                        'error' => 'Bad Request',
                        'status' => Response::HTTP_BAD_REQUEST,
                        'message' => $event->getThrowable()->getMessage()
                    ], Response::HTTP_BAD_REQUEST)
                );
                break;
            default:
                $event->setResponse(
                    new JsonResponse([
                        'error' => 'Unknown Error',
                        'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                        'message' => $event->getThrowable()->getMessage()
                    ], Response::HTTP_INTERNAL_SERVER_ERROR)
                );
        }
    }
}