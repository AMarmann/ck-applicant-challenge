<?php


namespace App\Exception;

use LogicException;

/**
 * Class ToDoNotFoundException
 */
class ToDoNotFoundException extends LogicException
{

}