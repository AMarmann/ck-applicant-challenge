<?php


namespace App\Service;

use App\DTO\CreateToDo;
use App\DTO\OverwriteToDo;
use App\Entity\Task;
use App\Entity\ToDo;
use App\Exception\ToDoNotFoundException;
use App\Repository\ToDoRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ToDoService
 */
class ToDoService
{
    private EntityManagerInterface $entityManager;
    private ToDoRepository $toDoRepository;

    /**
     * ToDoService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ToDoRepository $toDoRepository
     */
    public function __construct(EntityManagerInterface $entityManager, ToDoRepository $toDoRepository)
    {
        $this->entityManager = $entityManager;
        $this->toDoRepository = $toDoRepository;
    }

    /**
     * Return all To-Dos and their subtasks from the database.
     *
     * @return array
     */
    public function getAllToDos(): array
    {
        $toDos = $this->toDoRepository->findAll();

        if (!$toDos)
            throw new ToDoNotFoundException(
                'Keine To-Dos vorhanden.'
            );

        return $toDos;
    }

    /**
     * Search a To-Do by a given id and return it.
     *
     * @param int $id
     * @return ToDo
     */
    public function getToDoById(int $id): ToDo
    {
        $toDo = $this->toDoRepository->find($id);

        if (!$toDo)
            throw new ToDoNotFoundException(
                'Kein To-Do mit der ID ' . $id . ' gefunden.'
            );

        return $toDo;
    }

    /**
     * Create a new To-Do with its optional tasks and persists it to the database
     *
     * @param CreateToDo $createToDo
     * @return ToDo
     */
    public function createNewToDo(CreateToDo $createToDo): ToDo
    {
        $tasks = [];
        foreach ($createToDo->getTasks() as $task)
            $tasks[] = Task::create($task['name'], $task['description']);

        // Create and persist To-Do.
        $toDo = ToDo::create($createToDo->getName(), $createToDo->getDescription(), $tasks);
        $this->entityManager->persist($toDo);
        $this->entityManager->flush();

        return $toDo;
    }

    /**
     * Update an existing To-Do with given values.
     *
     * @param OverwriteToDo $overwriteToDoDto
     * @return ToDo
     */
    public function overwriteToDo(OverwriteToDo $overwriteToDoDto): ToDo
    {
        $toDo = $this->toDoRepository->find($overwriteToDoDto->getId());

        if (!$toDo)
            throw new ToDoNotFoundException(
                'Kein To-Do mit der ID ' . $overwriteToDoDto->getId() . ' gefunden.'
            );

        $tasks = [];
        foreach ($overwriteToDoDto->getTasks() as $task)
            $tasks[] = Task::create($task['name'], $task['description']);

        $toDo->overwrite($overwriteToDoDto->getName(), $overwriteToDoDto->getDescription(), $tasks);
        $this->entityManager->persist($toDo);
        $this->entityManager->flush();

        return $toDo;
    }

    /**
     * Delete an existing To-Do and its subtasks.
     *
     * @param int $id
     */
    public function deleteToDo(int $id)
    {
        $toDo = $this->toDoRepository->find($id);
        if (!$toDo)
            throw new ToDoNotFoundException(
                'Kein To-Do mit der ID ' . $id . ' gefunden.'
            );

        $this->entityManager->remove($toDo);
        $this->entityManager->flush();
    }
}