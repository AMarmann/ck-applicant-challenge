<?php


namespace App\Controller;

use App\Dto\CreateToDo;
use App\Dto\OverwriteToDo;
use App\Service\ToDoService;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ToDoController extends AbstractController
{
    private ToDoService $toDoService;

    /**
     * ToDoController constructor.
     * @param ToDoService $toDoService
     */
    public function __construct(ToDoService $toDoService)
    {
        $this->toDoService = $toDoService;
    }

    /**
     * Returns an existing To-Do and its subtasks.
     *
     * @Route("/todos/{id}", methods={"GET"}, name="get_to_do")
     * @param int $id
     * @return JsonResponse
     */
    public function getToDo(int $id): JsonResponse
    {
        $toDo = $this->toDoService->getToDoById($id);

        return $this->json($toDo, Response::HTTP_OK);
    }

    /**
     * @Route("/todos", methods={"GET"}, name="get_all_to_dos")
     */
    public function getAllToDos(): JsonResponse
    {
        $todos = $this->toDoService->getAllToDos();

        return $this->json($todos, Response::HTTP_OK);
    }

    /**
     * @Route("/todos", methods={"POST"}, name="create_to_do")
     * @param Request $request
     * @return JsonResponse
     */
    public function createToDo(Request $request): JsonResponse
    {
        $data = $this->decodeJson($request);
        $validData = $this->validateRequestSchema($data);
        $createToDoDto = CreateToDo::createFromRequestContent($validData);

        $todo = $this->toDoService->createNewToDo($createToDoDto);

        return $this->json($todo, Response::HTTP_CREATED);
    }

    /**
     * Overwrites an existing To-Do and its subtasks with given values.
     *
     * @Route("/todos/{id}", methods={"PUT"}, name="overwrite_to_do")
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function overwriteToDo(Request $request, int $id)
    {
        $data = $this->decodeJson($request);
        $validData = $this->validateRequestSchema($data);
        $overwriteToDoDto = OverwriteToDo::createFromRequestContent($id, $validData);

        $todo = $this->toDoService->overwriteToDo($overwriteToDoDto);

        return $this->json($todo, Response::HTTP_OK);
    }

    /**
     * Deletes an existing To-Do and its subtasks.
     *
     * @Route("/todos/{id}", methods={"DELETE"}, name="delete_to_do")
     * @param int $id
     * @return JsonResponse
     */
    public function deleteToDo(int $id): JsonResponse
    {
        $this->toDoService->deleteToDo($id);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Decodes a JSON Request and throws a InvalidArgumentException it is not valid
     *
     * @param Request $request
     * @return mixed
     * @throws InvalidArgumentException
     */
    private function decodeJson(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null)
            throw new InvalidArgumentException('JSON ist nicht valide.');

        return $data;
    }

    /**
     * @param $request
     * @return array
     */
    private function validateRequestSchema($request)
    {
        // To-Do name is required
        if (!isset($request['name']) || empty($request['name']))
            throw new InvalidArgumentException('To-Do-Name ist erforderlich.');
        $name = $request['name'];

        // To-Do description is optional
        $description = $request['description'] ?? '';

        $tasks = [];
        // Initialize subtasks from request data if keyword 'tasks' is present and not empty.
        if (isset($request['tasks']) && sizeof($request['tasks']) > 0) {
            foreach ($request['tasks'] as $task) {
                // Subtask name is required
                if (!isset($task['name']) || empty($task['name']))
                    throw new InvalidArgumentException('Subtask-Name ist erforderlich.');

                $taskDescription = $task['description'] ?? '';
                $tasks[] = ['name' => $task['name'], 'description' => $taskDescription];
            }
        }

        return ['name' => $name, 'description' => $description, 'tasks' => $tasks];
    }
}