<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JsonSerializable;

/**
 * This class represents a Subtask for a To-Do.
 *
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="text")
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * Task constructor.
     * @param $name
     * @param $description
     */
    public function __construct($name, $description)
    {
        $this->id = null;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * Creates a new Task.
     * Task id will be set when inserting in database.
     *
     * @param string $name
     * @param string|null $description
     * @return Task
     */
    public static function create(string $name, string $description = null)
    {
        if (empty($name))
            throw new InvalidArgumentException('Subtask-Name ist erforderlich');

        return new self($name, $description);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Serializes entity values.
     *
     * @return array
     */
    public function jsonSerialize() : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description
        ];
    }
}
