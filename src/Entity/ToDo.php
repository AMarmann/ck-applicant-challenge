<?php

namespace App\Entity;

use App\Repository\ToDoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=ToDoRepository::class)
 */
class ToDo implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="text")
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * One To-Do can have many Tasks.
     * @ORM\ManyToMany(targetEntity="App\Entity\Task", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinTable(name="to_do_task",
     *     joinColumns={@ORM\JoinColumn(name="to_do_id", referencedColumnName="id", onDelete="cascade")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id", unique=true)})
     */
    public ?Collection $tasks;

    /**
     * ToDo constructor.
     * @param $name
     * @param $description
     * @param $tasks
     */
    public function __construct($name, $description, $tasks)
    {
        $this->id = null;
        $this->name = $name;
        $this->description = $description;
        $this->tasks = new ArrayCollection($tasks);
    }

    /**
     * Creates a new To-Do with its subtasks.
     * A To-Do name is mandatory for creation
     *
     * @param string $name
     * @param string|null $description
     * @param array|null $tasks
     * @return ToDo
     */
    public static function create(string $name, string $description = null, array $tasks = []): self
    {
        if (empty($name))
            throw new InvalidArgumentException('To-Do-Name ist erforderlich');

        return new self($name, $description, $tasks);
    }

    /**
     * @param string $name
     * @param string|null $description
     * @param array|null $tasks
     * @return $this
     */
    public function overwrite(string $name, string $description = null, array $tasks = []): self
    {
        $this->name = $name;
        $this->description = $description;

        $this->tasks->clear();
        foreach ($tasks as $task)
            $this->tasks->add($task);

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getTasks(): array
    {
        return $this->tasks->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $serializedTasks = [];
        foreach ($this->tasks as $task)
            $serializedTasks[] = $task->jsonSerialize();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'tasks' => $serializedTasks
        ];
    }

}
