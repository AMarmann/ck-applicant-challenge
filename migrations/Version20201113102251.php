<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113102251 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE task (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name CLOB NOT NULL, description CLOB DEFAULT NULL)');
        $this->addSql('CREATE TABLE to_do (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name CLOB NOT NULL, description CLOB DEFAULT NULL)');
        $this->addSql('CREATE TABLE to_do_task (to_do_id INTEGER NOT NULL, task_id INTEGER NOT NULL, PRIMARY KEY(to_do_id, task_id))');
        $this->addSql('CREATE INDEX IDX_5CD66BD15BE9ECD7 ON to_do_task (to_do_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5CD66BD18DB60186 ON to_do_task (task_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE to_do');
        $this->addSql('DROP TABLE to_do_task');
    }
}
