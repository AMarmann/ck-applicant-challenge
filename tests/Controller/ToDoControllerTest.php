<?php

namespace App\Tests\Controller;

use App\Dto\CreateToDo;
use App\Dto\OverwriteToDo;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ToDoControllerTest extends WebTestCase
{
    /** @var KernelBrowser */
    private KernelBrowser $client;

    /** @var EntityManager */
    private EntityManager $entityManager;

    public function validRequestProvider()
    {
        return [
            ['{"name":"Frühjahrsputz1"}'],
            ['{"name":"Frühjahrsputz2", "description":""}'],
            ['{"name":"Frühjahrsputz3", "description":"", "tasks": []}'],
            ['{"name":"Frühjahrsputz4", "description": "Gesamte Wohnung aufräumen und putzen", "tasks":[{"name": "Fenster putzen"}, {"name": "Kleiderschrank ausmisten", "description": "Alles raus, was nicht passt"}]}'],
        ];
    }

    public function invalidRequestProvider()
    {
        return [
            ['{name":"Frühjahrsputz"}', Response::HTTP_BAD_REQUEST, 'JSON ist nicht valide.'],
            ['{}', Response::HTTP_BAD_REQUEST, 'To-Do-Name ist erforderlich.'],
            [
                '{"name":"", "description": "Gesamte Wohnung aufräumen und putzen", "tasks":[{"name": "Fenster putzen"}, {"name": "Kleiderschrank ausmisten", "description": "Alles raus, was nicht passt"}]}',
                Response::HTTP_BAD_REQUEST, 'To-Do-Name ist erforderlich.'
            ],
            [
                '{"name":"Frühjahrsputz", "description": "Gesamte Wohnung aufräumen und putzen", "tasks":[{"description": "Alles raus, was nicht passt"}]}',
                Response::HTTP_BAD_REQUEST, 'Subtask-Name ist erforderlich.'
            ],
            [
                '{"name":"Frühjahrsputz", "description": "Gesamte Wohnung aufräumen und putzen", "tasks":[{"name": ""}, {"name": "Kleiderschrank ausmisten", "description": "Alles raus, was nicht passt"}]}',
                Response::HTTP_BAD_REQUEST, 'Subtask-Name ist erforderlich.'
            ],
        ];
    }

    public function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->disableReboot();
        $this->entityManager = self::$kernel->getContainer()->get('doctrine')->getManager();
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @test
     * @dataProvider invalidRequestProvider
     * @param $invalidRequest
     * @param $errorCode
     * @param $errorMessage
     */
    public function createToDoShouldFailIfRequestIsInvalid($invalidRequest, $errorCode, $errorMessage)
    {
        $this->client->request(
            'POST',
            '/todos',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $invalidRequest
        );

        $this->assertEquals($errorCode, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString($errorMessage, $this->client->getResponse()->getContent());
    }

    /**
     * @test
     * @dataProvider validRequestProvider
     * @param $request
     */
    public function createToDoShouldSuccessfullyReturnToDoAsJson($request)
    {
        $this->client->request(
            'POST',
            '/todos',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $request
        );

        $this->assertEquals(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());
        $this->assertJson($this->client->getResponse()->getContent());

        $response = json_decode($this->client->getResponse()->getContent(), true);
        $createToDoDto = CreateToDo::createFromRequestContent($this->validateRequestSchema(json_decode($this->client->getRequest()->getContent(), true)));

        $this->assertNotNull($response['id']);
        $this->assertEquals($createToDoDto->getName(), $response['name']);
        $this->assertEquals($createToDoDto->getDescription(), $response['description']);
        $this->assertEquals(sizeof($createToDoDto->getTasks()), sizeof($response['tasks']));

        foreach ($response['tasks'] as $task) {
            $this->assertNotNull($task['id']);
            $expectedTaskIndex = array_search($task['name'], array_column($createToDoDto->getTasks(), 'name'));
            $this->assertEquals($createToDoDto->getTasks()[$expectedTaskIndex]['description'], $task['description']);
        }
    }

    /**
     * @test
     */
    public function getAllToDosShouldFailIfRepositoryIsEmpty()
    {
        $this->client->request('GET', '/todos');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getAllToDosShouldReturnAllToDosAsJson()
    {
        $this->createFourToDos();
        $this->client->request('GET', '/todos');

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertJson($this->client->getResponse()->getContent());

        $jsonResponse = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(4, sizeof($jsonResponse));
    }

    /**
     * @test
     */
    public function getToDoShouldFailIfRequestedIdDoesNotExist()
    {
        $this->client->request('GET', '/todos/2000');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getToDoShouldReturnAnExistingToDoAsJson()
    {
        $this->createToDo();
        $this->client->request('GET', '/todos/1');

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertJson($this->client->getResponse()->getContent());
    }

    /**
     * @test
     */
    public function overwriteToDoShouldFailIfRequestedIdDoesNotExist()
    {
        $this->client->request(
            'PUT',
            '/todos/1',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $this->validRequestProvider()[1][0]
        );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function overwriteToDoShouldReturnUpdatedToDoAsJson()
    {
        $this->createToDo();

        $this->client->request(
            'PUT',
            '/todos/1',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $this->validRequestProvider()[1][0]
        );

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertJson($this->client->getResponse()->getContent());

        $response = json_decode($this->client->getResponse()->getContent(), true);
        $createToDoDto = OverwriteToDo::createFromRequestContent(1, $this->validateRequestSchema(json_decode($this->client->getRequest()->getContent(), true)));

        $this->assertNotNull($response['id']);
        $this->assertEquals($createToDoDto->getName(), $response['name']);
        $this->assertEquals($createToDoDto->getDescription(), $response['description']);
        $this->assertEquals(sizeof($createToDoDto->getTasks()), sizeof($response['tasks']));

        foreach ($response['tasks'] as $task) {
            $this->assertNotNull($task['id']);
            $expectedTaskIndex = array_search($task['name'], array_column($createToDoDto->getTasks(), 'name'));
            $this->assertEquals($createToDoDto->getTasks()[$expectedTaskIndex]['description'], $task['description']);
        }
    }

    /**
     * @test
     */
    public function deleteToDoShouldFailIfRequestedIdDoesNotExist()
    {
        $this->client->request('DELETE', '/todos/2000');
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function deleteToDoShouldDeleteAnExistingToDoAndReturnEmptyResponse()
    {
        $this->createToDo();
        $this->client->request('DELETE', '/todos/1');

        $this->assertEquals(Response::HTTP_NO_CONTENT, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(null, json_decode($this->client->getResponse()->getContent(), true));
    }

    /**
     * Rollback transaction changes.
     * @throws ConnectionException
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->getConnection()->rollBack();
    }

    /**
     * Helper function to create four to dos and insert them in the repository.
     */
    private function createToDo()
    {
        $this->client->request(
            'POST',
            '/todos',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $this->validRequestProvider()[0][0]
        );
    }

    /**
     * Helper function to create four to dos and insert them in the repository.
     */
    private function createFourToDos()
    {
        foreach ($this->validRequestProvider() as $request)
            $this->client->request(
                'POST',
                '/todos',
                [],
                [],
                ['CONTENT_TYPE' => 'application/json'],
                $request[0]
            );
    }

    /**
     * Helper function to generate expected values for request.
     * @param $request
     * @return array
     */
    private function validateRequestSchema($request): array
    {
        $name = $request['name'];

        // To-Do description is optional
        $description = $request['description'] ?? '';

        $tasks = [];
        // Initialize subtasks from request data if keyword 'tasks' is present and not empty.
        if (isset($request['tasks']) && sizeof($request['tasks']) > 0) {
            foreach ($request['tasks'] as $task) {
                $taskDescription = $task['description'] ?? '';
                $tasks[] = ['name' => $task['name'], 'description' => $taskDescription];
            }
        }

        return ['name' => $name, 'description' => $description, 'tasks' => $tasks];
    }
}
