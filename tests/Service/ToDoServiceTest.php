<?php

namespace App\Tests\Service;

use App\Dto\CreateToDo;
use App\Dto\OverwriteToDo;
use App\Entity\ToDo;
use App\Exception\ToDoNotFoundException;
use App\Service\ToDoService;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ToDoServiceTest extends KernelTestCase
{
    private ToDoService $toDoService;
    private EntityManager $entityManager;
    private $toDoRepository;
    private CreateToDo $createToDoDto;

    protected function setUp()
    {
        parent::setUp();
        self::bootKernel();
        $this->entityManager = self::$kernel->getContainer()->get('doctrine')->getManager();
        $this->toDoRepository = self::$kernel->getContainer()->get('doctrine.orm.entity_manager')->getRepository(ToDo::class);
        $this->toDoService = new ToDoService($this->entityManager, $this->toDoRepository);
        $this->entityManager->getConnection()->beginTransaction();

        $this->createToDoDto = new CreateToDo('Frühjahrsputz', 'Gesamte Wohnung putzen', $this->tasksProvider());
    }

    /**
     * @test
     */
    public function createNewToDoShouldSuccessfullyPersistANewToDo()
    {
        $toDo = $this->toDoService->createNewToDo($this->createToDoDto);

        // Check whether created to-do has been correctly persisted
        $toDoFromDatabase = $this->toDoRepository->find($toDo->getId());
        $this->assertNotNull($toDoFromDatabase);
        $this->assertEquals($this->createToDoDto->getName(), $toDoFromDatabase->getName());
        $this->assertEquals($this->createToDoDto->getDescription(), $toDoFromDatabase->getDescription());
        $this->assertEquals(sizeof($toDo->getTasks()), sizeof($toDoFromDatabase->getTasks()));

        foreach ($toDoFromDatabase->getTasks() as $task)
        {
            $this->assertNotNull($task->getId());
        }
    }

    /**
     * @test
     */
    public function deleteToDoShouldFailIfToDoDoesNotExist()
    {
        $this->expectException(ToDoNotFoundException::class);
        $this->expectExceptionMessage('Kein To-Do mit der ID '. $this->invalidID() . ' gefunden.');
        $this->toDoService->deleteToDo($this->invalidID());

    }

    /**
     * @test
     */
    public function deleteToDoShouldRemoveAnExistingToDoFromTheRepository()
    {
        $id = $this->toDoService->createNewToDo($this->createToDoDto)->getId();

        $this->toDoService->deleteToDo($id);
        $toDoFromDatabase = $this->toDoRepository->find($id);
        $this->assertNull($toDoFromDatabase);
    }

    /**
     * @test
     */
    public function getToDoByIdShouldFailIfIdDoesNotExist()
    {
        $this->expectException(ToDoNotFoundException::class);
        $this->expectExceptionMessage('Kein To-Do mit der ID '. $this->invalidID() . ' gefunden.');
        $this->toDoService->getToDoById($this->invalidID());
    }

    /**
     * @test
     */
    public function getToDoByIdShouldReturnToDoAndTasks()
    {
        $expectedToDo = $this->toDoService->createNewToDo($this->createToDoDto);

        $toDo = $this->toDoService->getToDoById($expectedToDo->getId());
        $this->assertNotNull($toDo);
        $this->assertInstanceOf(ToDo::class, $toDo);

        $this->assertEquals($expectedToDo->getName(), $toDo->getName());
        $this->assertEquals($expectedToDo->getDescription(), $toDo->getDescription());
        $this->assertEquals($expectedToDo->getTasks(), $toDo->getTasks());
    }

    /**
     * @test
     */
    public function getAllToDosShouldFailIfRepositoryIsEmpty()
    {
        $this->expectException(ToDoNotFoundException::class);
        $this->expectExceptionMessage('Keine To-Dos vorhanden.');
        $this->toDoService->getAllToDos();
    }

    /**
     * @test
     */
    public function getAllToDosShouldReturnAllPersistedToDos()
    {
        $this->createThreeToDos();

        $expectedToDos = $this->toDoRepository->findAll(ToDo::class);
        $toDos = $this->toDoService->getAllToDos();

        $this->assertEquals(sizeof($expectedToDos), sizeof($toDos));
        $this->assertEquals($expectedToDos, $toDos);
    }

    /**
     * @test
     */
    public function overwriteToDoShouldFailIfGivenIdDoesNotExist()
    {
        $this->expectException(ToDoNotFoundException::class);
        $this->expectExceptionMessage('Kein To-Do mit der ID ' . $this->invalidID(). ' gefunden.');

        $this->toDoService->overwriteToDo($this->overwriteToDoDto($this->invalidID()));
    }

    /**
     * @test
     */
    public function overwriteToDoShouldOverwriteAnExistingToDoAndItsTasks()
    {
        $id = $this->toDoService->createNewToDo($this->createToDoDto)->getId();
        $overwriteToDoDto = $this->overwriteToDoDto($id);

        $this->toDoService->overwriteToDo($overwriteToDoDto);
        $newToDo = $this->toDoRepository->find($id);

        $this->assertEquals($overwriteToDoDto->getName(), $newToDo->getName());
        $this->assertEquals($overwriteToDoDto->getDescription(), $newToDo->getDescription());
        $this->assertEquals(sizeof($overwriteToDoDto->getTasks()), sizeof($newToDo->getTasks()));
    }

    /**
     * Rollback transaction changes.
     * @throws ConnectionException
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->getConnection()->rollBack();
    }

    private function invalidID():int
    {
        return 2000;
    }

    /**
     * @return array
     */
    private function tasksProvider(): array
    {
        return [
            ['name' => 'Fenster putzen', 'description' => 'Alle Fenster mit Glasreiniger putzen'],
            ['name' => 'Wohnzimmer staubsaugen', 'description' => ""],
            ['name' => 'Wohnzimmerboden wischen', 'description' => 'Nass wischen']
        ];
    }

    private function overwriteToDoDto(int $id): OverwriteToDo
    {
        return new OverwriteToDo($id, 'Wohnung renovieren', '', [['name' => 'Wohnzimmer streichen', 'description' => 'Weiße Farbe verwenden.']]);
    }

    /**
     * Helper function to add some To-Dos to the repository.
     */
    private function createThreeToDos()
    {
        $this->toDoService->createNewToDo($this->createToDoDto);
        $this->toDoService->createNewToDo($this->createToDoDto);
        $this->toDoService->createNewToDo($this->createToDoDto);
    }
}
