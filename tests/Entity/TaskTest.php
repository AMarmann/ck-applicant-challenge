<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    /**
     * @return array
     */
    public function parameterProvider(): array
    {
        return [
            ['Fenster putzen', 'Alle Fenster mit Glasreiniger putzen.'],
            ['Wohnzimmer staubsaugen', null]
        ];
    }

    /**
     * @test
     */
    public function createShouldFailIfNameIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Subtask-Name ist erforderlich');
        Task::create('', 'Nass wischen.');
    }

    /**
     * @test
     * @dataProvider parameterProvider
     * @param $name
     * @param $description
     */
    public function createShouldSuccessfullyCreateANewTask($name, $description)
    {
        $task = Task::create($name, $description);
        $this->assertEquals($name, $task->getName());
        $this->assertEquals($description, $task->getDescription());
    }


    /**
     * @test
     */
    public function createShouldSuccessfullyCreateANewTaskWithoutADescriptionGiven()
    {
        $name = 'Mülleimer leeren';
        $task = Task::create($name);
        $this->assertEquals($name, $task->getName());
        $this->assertEquals(null, $task->getDescription());
    }

    /**
     * @test
     * @dataProvider parameterProvider
     * @param $name
     * @param $description
     */
    public function jsonSerializeShouldSuccessfullyTransformTaskToJson($name, $description)
    {
        $task = Task::create($name, $description);

        $expectedJson = ['id' => null, 'name' => $name, 'description' => $description];
        $actualJson = $task->jsonSerialize();

        $this->assertEquals($expectedJson, $actualJson);
    }
}
