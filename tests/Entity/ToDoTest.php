<?php

namespace App\Tests\Entity;

use App\Entity\Task;
use App\Entity\ToDo;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ToDoTest extends TestCase
{
    /**
     * @return array
     */
    public function parameterProviderWithoutTasks(): array
    {
        return [
            ['Frühjahrsputz', 'Gesamte Wohnung putzen und entrümpeln.'],
            ['Wohnzimmer staubsaugen', null]
        ];
    }

    /**
     * @return array
     */
    public function parameterProviderWithTasks(): array
    {
        return [
            ['Frühjahrsputz', 'Gesamte Wohnung putzen und entrümpeln.', $this->tasksProvider()],
            ['Wohnzimmer staubsaugen', null, $this->tasksProvider()]
        ];
    }

    /**
     * @return array
     */
    public function tasksProvider(): array
    {
        return [
            Task::create('Fenster putzen', 'Alle Fenster mit Glasreiniger putzen'),
            Task::create('Wohnzimmer staubsaugen'),
            Task::create('Wohnzimmerboden wischen', 'Nass wischen')
        ];
    }

    /**
     * @test
     */
    public function createShouldFailIfNameIsEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('To-Do-Name ist erforderlich');
        ToDo::create('', 'Gesamte Wohnung putzen.');
    }

    /**
     * @test
     * @dataProvider parameterProviderWithoutTasks
     * @param $name
     * @param $description
     */
    public function createShouldCreateANewToDoWithoutSubtasks($name, $description)
    {
        $todo = ToDo::create($name, $description);
        $this->assertEquals($name, $todo->getName());
        $this->assertEquals($description, $todo->getDescription());
        $this->assertEquals(0, sizeof($todo->getTasks()));
    }

    /**
     * @test
     * @dataProvider parameterProviderWithTasks
     * @param $name
     * @param $description
     * @param $tasks
     */
    public function createShouldCreateANewToDoWithSubtasks($name, $description, $tasks)
    {
        $todo = ToDo::create($name, $description, $tasks);
        $this->assertEquals($name, $todo->getName());
        $this->assertEquals($description, $todo->getDescription());

        $this->assertNotNull($todo->getTasks());
        $this->assertEquals(sizeof($tasks), sizeof($todo->getTasks()));
        $this->assertEquals($tasks, $todo->getTasks());
    }

    /**
     * @dataProvider parameterProviderWithTasks
     * @test
     * @param $newName
     * @param $newDescription
     * @param $newTasks
     */
    public function overwriteShouldSuccessfullyReplaceAnExistingToDo($newName, $newDescription, $newTasks)
    {
        $oldTasks = array(Task::create('Gewürze ordnen', 'Leere Gewürze auffüllen'));
        $toDo = ToDo::create('Küche aufräumen', 'Arbeitsfläche ordnen', $oldTasks);

        $toDo->overwrite($newName, $newDescription, $newTasks);

        $this->assertEquals($newName, $toDo->getName());
        $this->assertEquals($newDescription, $toDo->getDescription());
        $this->assertEquals($newTasks, $toDo->getTasks());
    }

    /**
     * @test
     * @dataProvider parameterProviderWithTasks
     * @param $name
     * @param $description
     * @param $tasks
     */
    public function jsonSerializeShouldSuccessfullySerializeToDo($name, $description, $tasks)
    {
        $todo = ToDo::create($name, $description, $tasks);

        $serializedTasks = [];
        foreach ($tasks as $task)
            $serializedTasks[] = $task->jsonSerialize();

        $expectedSerialized = ['id' => null, 'name' => $name, 'description' => $description, 'tasks' => $serializedTasks];
        $actualSerialized = $todo->jsonSerialize();

        $this->assertIsArray($actualSerialized);
        $this->assertEquals($expectedSerialized, $actualSerialized);
    }

}
