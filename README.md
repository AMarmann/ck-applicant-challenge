Dieses Projekt enhält eine REST-Api zur Erstellung und Verwaltung von To-Dos.

**Voraussetzungen:**

PHP 7.4 oder höher
Symfony 5.1 Voraussetzungen müssen erfüllt sein: https://symfony.com/doc/current/setup.html


**Installation:**

Abhängigkeiten können mit composer installiert werden.
`composer install`


**Usage:**

Das Projekt nutzt den integrierten Webserver von Symfony.
Zum Starten des Servers:
`$symfony serve`

**Routes**

`GET /todos → Returns a list of all Todos`

`POST /todos → Expects a Todo (without id) and returns a Todo with id`

Erwartet einen Json-Request-Body nach folgendem Schema:
```
{
    "name": "string"
    "description": "string"
    "tasks": [
        {
            "name": "string",
            "description": "string"
        }
    ]
}
```



`GET /todos/{id} → Returns a Todo`

`PUT /todos/{id} → Overwrites an existing Todo`

Erwartet einen Json-Request-Body nach folgendem Schema:
```
{
    "name": "string"
    "description": "string"
    "tasks": [
        {
            "name": "string",
            "description": "string"
        }
    ]
}
```

`DELETE /todos/{id} → Deletes a Todo`

**Organisation**
Ein ToDo besteht aus folgender Struktur:
`{
    id [mandatory]
    name [mandatory]
    description
    tasks: [
        {
            id [mandatory]
            name [mandatory]
            description
        }
    ]
}`

Ein Task wird als Subtask eines ToDos verstanden und ist damit ein Entity des Aggregates ToDo. Die Anzahl an Tasks ist beliebig.